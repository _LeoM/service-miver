function sendRequest(name,value){
    let xhttp = new XMLHttpRequest(); 
    xhttp.open("POST", "/php/requestManager.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let toSend = "";
    for (let i = 0; i < name.length; i++) {
      toSend += `${name[i]}=${value[i]}`;
      if(i != name.length-1){
        toSend += "&";
      }
    }
    console.log(toSend);
    xhttp.send(toSend);
  }