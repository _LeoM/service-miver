function stopAndStart(id, checkBox){
    if(!checkBox.checked){
        sendRequest(['stop'],[id])
    }else {
        sendRequest(['start'],[id])
    }
}

function crackOnOff(user, port, id, checkBox){
    if(!checkBox.checked){
        sendRequest(['upd',"params", "file", "nv"],[id,"online-mode",`/serveur/serv/${user}/${port}/server.properties`,"true"])
    }else {
        sendRequest(['upd',"params", "file", "nv"],[id,"online-mode",`/serveur/serv/${user}/${port}/server.properties`,"false"])
    }
}