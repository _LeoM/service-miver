<?php

session_start();

function genServ($version) {

    $last_port = $minServPort-1;

    $minServPort = 50000;
    $maxServPort = 50010;

    $servPort = $minServPort;
    $name = "serv";

    $pdo = new PDO('mysql:host=database;dbname=miver_db', 'root', 'rootpassword');

    $query = "SELECT Port FROM serveur";

    $stmt = $pdo->query($query);
    while($row = $stmt->fetch()){
        if($row['Port'] != $last_port+1){
            break;
        }else {
            $last_port = $row['Port'];
            $servPort = $last_port + 1;
        }
    }
    
    $rconPort = $servPort + 1000;
    $queryPort = $servPort + 2000;
    $name .= (string) $servPort-$minServPort;

    if($servPort > $maxServPort || $servPort < $minServPort){
    }else {
        $query = "INSERT INTO serveur value(?, ?, ?, ?, ?, ?)";
        $stmt = $pdo->prepare($query);
        $stmt->bindParam(1, $_SESSION['user'], PDO::PARAM_STR);
        $stmt->bindParam(2, $name, PDO::PARAM_STR);
        $stmt->bindParam(3, $servPort, PDO::PARAM_INT);
        $stmt->bindParam(4, $rconPort, PDO::PARAM_INT);
        $stmt->bindParam(5, $queryPort, PDO::PARAM_INT);
        $stmt->bindParam(6, $version, PDO::PARAM_STR);
        $stmt->execute();
    
        // Gen serv whit propertiesFile and good version
        $p = $_SESSION['user'];
        $cmd = "sh creatServ.sh $name $servPort $rconPort $queryPort $version $p";
        shell_exec($cmd);
    }

    $pdo = null;
}

function startServ($id){
    shell_exec("docker start $id");
    exit();
}

function stopServ($id){
    shell_exec("docker stop $id");
    exit();
}

function restartServ($id){
    shell_exec("docker restart $id");
    exit();
}

function commandToServ($id, $command){
    shell_exec("docker exec $id rcon-cli $command");
    exit();
}

function modifParam($id, $param, $file, $new_val){
    $regex = "/(?<=$param=).*/";
    $filecontent = file_get_contents($file);
    file_put_contents($file,preg_replace($regex, $new_val, $filecontent));
    exit();
}

if (isset($_POST['creat']) && isset($_SESSION['user'])) {
    genServ($_POST['version']);
    header("Location: ../serverDashboard.php");
    exit();
}else {
    header("Location: ../index.php");
}

if (isset($_POST['start']) && isset($_SESSION['user'])) {
    startServ($_POST['start']);
}

if (isset($_POST['stop']) && isset($_SESSION['user'])) {
    stopServ($_POST['stop']);
}

if (isset($_POST['reload']) && isset($_SESSION['user'])) {
    execCommand("reload");
    restartServ($_POST['reload']);
}

if (isset($_POST['cmd']) && isset($_SESSION['user'])) {
    commandToServ($_POST['id'], $_POST['cmd']);
}

if (isset($_POST['upd']) && isset($_SESSION['user'])) {
    modifParam($_POST['upd'], $_POSTPOST['params'], $_POST['file'], $_POST['nv']);
}

?>
