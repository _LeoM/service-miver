#!/bin/bash

docker run --name $1 -v "/srv/miver-serveur/serv/$6/$2:/data" -d -it -p $2:25565 -p $3:25575 -p $4:25585 -e QUERY_PORT=25585 -e ENABLE_QUERY=true -e EULA=TRUE -e VERSION=$5 -e SERVER_NAME=Miver -e MOTD="§4Miver Serveur" -e RCON_PASSWORD=$6 itzg/minecraft-server

