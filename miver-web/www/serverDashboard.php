<?php

session_start();
if(!isset($_SESSION['user'])){
    header("location: index.php");
    exit();
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/index_style.css">
    <link rel="stylesheet" href="/css/scrollbar_style.css">
    <link rel="stylesheet" href="/css/servModif_style.css">
    <link rel="stylesheet" href="/css/console_style.css">
    <link rel="icon" href="">
    <link href="https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css" rel="stylesheet">
    <title>Miver</title>
</head>
<body>
    
<div class="header" oneclick="window.location.href='index.php'">
    <div class="logo" onclick="window.location.href='/'">
        <h2><a href="/">Miver</a></h2>
    </div>
    <div class="right">
        
    </div>
</div>
<div class="content">
    <?php

    $username = $_SESSION['user'];

    $pdo = new PDO('mysql:host=database;dbname=miver_db', 'root', 'rootpassword');

    $query = "SELECT Name,Port,Version from serveur WHERE User='$username'";

    $stmt = $pdo->query($query);
    while($row = $stmt->fetch()){
        echo "<div class='servs'>
    <div class='top'>
        <h1 class='title'>90.120.45.87:".$row["Port"]."</h1>
        <h2 class='subtitle'>".$row["Version"]."</h2>
        <div class='but'>
            <input type='checkbox' id='btn".$row["Port"]."' onchange='stopAndStart(`".$row["Name"]."`, this)' checked>
            <label for='btn".$row["Port"]."' class='on-off'>
                <div class='txt-off'>OFF</div>
                <div class='txt-on'>ON</div>
            </label>     
        </div>
    </div>
    <div class='bot'>
        <div class='left'>
            <input type='text' class='console' placeholder='Console' onchange='valideCommand(`".$row["Name"]."`,this)'>
        </div>
        <div class='right'>
            <p>Cracked</p><label class='toggle'>
                <input type='checkbox' onchange='crackOnOff(`".$_SESSION['user']."`,`".$row["Port"]."`,`".$row["Name"]."`, this)'>
                <span class='slider'></span>
                <span class='labels' data-on='ON' data-off='OFF'></span>
            </label>
            <p>White-List</p><label class='toggle'>
                <input type='checkbox'>
                <span class='slider'></span>
                <span class='labels' data-on='ON' data-off='OFF'></span>
            </label>
            <p>PvP</p><label class='toggle'>
                <input type='checkbox'>
                <span class='slider'></span>
                <span class='labels' data-on='ON' data-off='OFF'></span>
            </label>
            <p>Nether</p><label class='toggle'>
                <input type='checkbox'>
                <span class='slider'></span>
                <span class='labels' data-on='ON' data-off='OFF'></span>
            </label>
        </div>
    </div>
</div>";

    }
    
    ?>
</div>
<script src="/js/request.js"></script>
<script src="/js/console.js"></script>
<script src="/js/checkBoxInterract.js"></script>
</body>
</html>
