<?php
session_start();
if(isset($_SESSION['connect'])){
    if($_SESSION['connect']){
        header("location: index.php");
    }
}
$error = '';
if(isset($_POST['username'])){
    if(empty($_POST['username']) || empty($_POST['password'])){
        $error = "Username or Password is invalid";
    }else {
        $username = $_POST['username'];
        $password = $_POST['password'];

        $pdo = new PDO('mysql:host=database;dbname=miver_db', 'root', 'rootpassword');

        $query = "SELECT Username, Password from user WHERE Username=? AND Password=? LIMIT 1";

        $stmt = $pdo->prepare($query);
        $stmt->bindParam(1, $username, PDO::PARAM_STR);
        $stmt->bindParam(2, $password, PDO::PARAM_STR);
        $stmt->execute();

        if($stmt->fetch()){
            $_SESSION['user'] = $username;
            $_SESSION['connect'] = true;
            header("location: index.php");
        }else {
            $error = "Username or Password is invalid";
        }
        
        $pdo = null;

    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/index_style.css">
    <link rel="stylesheet" href="/css/scrollbar_style.css">
    <link rel="stylesheet" href="/css/connexion_style.css">
    <link rel="icon" href="">
    <link href="https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css" rel="stylesheet">
    <title>Miver</title>
</head>
<body> 
<div class="header" oneclick="window.location.href='index.php'">
    <div class="logo" onclick="window.location.href='/'">
        <h2><a href="/">Miver</a></h2>
    </div>
</div>

<div class="content">
    <div class="in">
        <div class="left">
            <p>Connexion</p>
            <form name="te" action="" method="post">
                <input type="text" name="username" class="Username-Email" placeholder="Username or Email">
                <input type="text" name="password" class="Password" placeholder="Password">
                <input type="submit" class="btn-login" value="Login"></input>
            </form>
        </div>
        <div class="right">
            <p>You don't have an account ?</p>
            <p>Create your account <a href="signUp.php">here</a></p>
        </div>
    </div>
</div>
</body>
</html>