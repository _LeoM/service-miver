<?php
session_start();
$pdo = new PDO('mysql:host=database;dbname=miver_db', 'root', 'rootpassword');

$query = "CREATE TABLE [IF NOT EXISTS] user(Username VARCHAR(100) NOT NULL UNIQUE, Password VARCHAR(100) NOT NULL, PRIMARY KEY(Username));";
$query2 = "CREATE TABLE [IF NOT EXISTS] serveur (User VARCHAR(100) NOT NULL, Name VARCHAR(100) NOT NULL UNIQUE, Port INTEGER NOT NULL UNIQUE, Rcon INTEGER UNIQUE, Query INTEGER UNIQUE, Version VARCHAR(100) NOT NULL, PRIMARY KEY(Name), FOREIGN KEY(User) REFERENCES user(Username));";

$stmt = $pdo->prepare($query);
$stmt->execute();

$stmt = $pdo->prepare($query2);
$stmt->execute();
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/index_style.css">
    <link rel="stylesheet" href="/css/scrollbar_style.css">
    <link href="https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css" rel="stylesheet">
    <title>Miver</title>
</head>
<body>
<div class="header" oneclick="window.location.href='index.php">
    <div class="logo" onclick="window.location.href='/'">
        <h2><a href="/">Miver</a></h2>
    </div>
    <div class="right">
        <?php 
        if(isset($_SESSION['user'])){
            echo "<!-- à mettre seulement quand connecté -->
            <a class='btn-spc' href='serverDashboard.php'><i class='bx bx-server' ></i> My Space</a>
            <a class='btn-out' href='php/logout.php'><i class='bx bx-power-off'></i></i> Disconnect</a>";
        }else {
            echo "<!-- à mettre seulement quand déconnecté -->
            <a class='btn-in' href='signIn.php'>Sign In</a>
            <a class='btn-up' href='signUp.php'>Sign Up</a>";

        }
        
        ?>
    </div>    
</div>

<div class="content">
    <div class="mid">
        <div class="title">
            <h1>Miver</h1>
            <a class="subtitle">The best way to create your own server</a>
        </div>
        <form class="genserv" action="/php/requestManager.php" method="post">
        <details class="versions">
            <summary class="radios">
                <input type="radio" name="version" id="default" title="Minecraft Versions" value="1.7.9" checked>
                <input type="radio" name="version" id="item1" title="1.7.9" value="1.7.9">
                <input type="radio" name="version" id="item2" title="1.8.9" value="1.8.8">
                <input type="radio" name="version" id="item3" title="1.9.4" value="1.9.4">
                <input type="radio" name="version" id="item4" title="1.11" value="1.11">
                <input type="radio" name="version" id="item5" title="1.19.4" value="1.19.4">
            </summary>
            <ul class="list">
                <li>
                    <label for="item1">
                        1.7.9
                        <span></span>
                    </label>
                </li>
                <li>
                    <label for="item2">1.8.9</label>
                </li>
                <li>
                    <label for="item3">1.9.4</label>
                </li>
                <li>
                    <label for="item4">1.11</label>
                </li>
                <li>
                    <label for="item5">1.19.4</label>
                </li>
            </ul>
        </details>
            <input type="submit" class="button btn-gen" name="creat" value="Generate Server">
        </form>
        <div class="data">
            <div class="allserv">
                <p class="dserv">0</p>
                <p class="serv">Server Online</p>
            </div>
            <p class="sep">|</p>
            <div class="allreg">
                <p class="dreg">0</p>
                <p class="registered">Accunt Registered</p>
            </div>
        </div>
    </div>
</div>
<script src="/js/request.js"></script>
</body>
</html>