<?php
session_start();
if(isset($_SESSION['connect'])){
    if($_SESSION['connect']){
        header("location: index.php");
    }
}
$error = '';
if(isset($_POST['pseudo'])){
    if(empty($_POST['pseudo']) || empty($_POST['psw']) || empty($_POST['psw-repeat'])){
        $error = "Username or Password is invalid";
    }else {
        if($_POST['psw'] == $_POST['psw-repeat']){

            $username = $_POST['pseudo'];
            $password = $_POST['psw'];
    
            $pdo = new PDO('mysql:host=database;dbname=miver_db', 'root', 'rootpassword');
    
            $query = "INSERT INTO user VALUE(?, ?)";
    
            $stmt = $pdo->prepare($query);
            $stmt->bindParam(1, $username, PDO::PARAM_STR);
            $stmt->bindParam(2, $password, PDO::PARAM_STR);
            $stmt->execute();
    
            $_SESSION['user'] = $username;
            $_SESSION['connect'] = true;
            header("location: index.php");
            
            $error = "Username or Password is invalid";
            
            
            $pdo = null;

        }

    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/index_style.css">
    <link rel="stylesheet" href="/css/scrollbar_style.css">
    <link rel="stylesheet" href="/css/register_style.css">
    <link rel="icon" href="">
    <link href="https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css" rel="stylesheet">
    <title>Miver</title>
</head>
<body>
    
<div class="header" oneclick="window.location.href='index.php'">
    <div class="logo" onclick="window.location.href='/'">
        <h2><a href="index.php">Miver</a></h2>
    </div>
</div>

<div class="content">
    <form name="te" action="" method="post">
        <div class="container">
            <h1>Register</h1>
            <hr>
            <div class="inputs">
                <label for="pseudo"><b>Pseudo</b></label>
                <input type="text" placeholder="Enter Pseudo" name="pseudo" id="pseudo" required>

                <label for="psw"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="psw" id="psw" required>

                <label for="psw-repeat"><b>Repeat Password</b></label>
                <input type="password" placeholder="Repeat Password" name="psw-repeat" id="psw-repeat" required>

                <button type="submit" class="registerbtn">Register</button>
            </div>
            <hr>
        </div>
        
        <div class="signin">
            <p>Already have an account ? <a href="signIn.php">Sign in</a></p>
        </div>
    </form>
</div>

</body>
</html>