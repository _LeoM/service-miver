#!bin/bash

# Firewall
systemctl enable firewalld && sudo systemctl start firewalld

# Open Ports for web site and ssh
monitoringPort=19999

firewall-cmd --zone=public --add-service=http --permanent
firewall-cmd --zone=public --add-service=https --permanent
firewall-cmd --zone=public --add-service=ssh --permanent
firewall-cmd --zone=public --add-port=$monitoringPort/tcp --permanent

echo "HTTP HTTPS ports : Open"

# Define minecraft ports range and open them
minPort=50000
maxPort=50010

for (( port=$minPort; port<=$maxPort; port++))
do
  firewall-cmd --zone=public --add-port=$port/tcp --permanent
done

echo "Minecraft ports : Open"

# Close not used ports
firewall-cmd --zone=public --remove-port=1-65535/tcp --permanent
firewall-cmd --zone=public --remove-port=1-65535/udp --permanent

firewall-cmd --reload

echo "Not used ports : Close"