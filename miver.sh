#!bin/bash

# Disable CTRL + ALT + SUPPR
echo "HandleLidSwitch=ignore
HandlePowerKey=ignore
HandleSecurityKey=ignore" > /etc/systemd/logind.conf

systemctl restart systemd-logind.service

echo "Disable : CTRL + ALT + SUPPR"


# Firewall
systemctl enable firewalld && sudo systemctl start firewalld

# Open Ports for web site and ssh
monitoringPort=19999

firewall-cmd --zone=public --add-service=http --permanent
firewall-cmd --zone=public --add-service=https --permanent
firewall-cmd --zone=public --add-service=ssh --permanent
firewall-cmd --zone=public --add-port=$monitoringPort/tcp --permanent

echo "HTTP HTTPS ports : Open"

# Define minecraft ports range and open them
minPort=50000
maxPort=50010

for (( port=$minPort; port<=$maxPort; port++))
do
  firewall-cmd --zone=public --add-port=$port/tcp --permanent
done

echo "Minecraft ports : Open"

# Close not used ports
firewall-cmd --zone=public --remove-port=1-65535/tcp --permanent
firewall-cmd --zone=public --remove-port=1-65535/udp --permanent

firewall-cmd --reload

echo "Not used ports : Close"


# Fail2Ban Installation

dnf install epel-release -y
dnf install fail2ban -y

echo "Fail2Ban Installed"

#Fail2Ban Configuration

systemctl start fail2ban
systemctl enable fail2ban

echo "[sshd]
enabled = true
logpath = /var/log/auth.log
maxretry = 3" > /etc/fail2ban/jail.d/custom.conf

systemctl restart fail2ban

echo "Fail2Ban Configurated"

# User service create
adduser miver

echo "User miver create"

# Docker installation
dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
dnf install docker-ce docker-ce-cli containerd.io
systemctl start docker
systemctl enable docker
usermod -aG docker miver

echo "Docker install and miver add to docker group"
echo "To be able to execute docker commands without sudo add your user to the docker group"

# Prepare Miver file
mv miver-monitoring /srv/
mv miver-php-build /srv/
mv miver-web /srv/
mv docker-compose.yml /srv/
mv miverChangePort.sh /srv/
mv miver.sh /srv/

cd /srv

chown -R miver miver-monitoring
chown -R miver miver-php-build
chown -R miver miver-web
chown miver docker-compose.yml

echo "Miver files move to /srv/ and own to miver"

# Execute container

sudo -u miver mkdir miver-serveur
sudo -u miver docker compose up -d

echo "Miver ready"
