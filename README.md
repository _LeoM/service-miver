<br>

# **Miver**
##### *Minecraft server hosting service*

<br>

## **Summary**

1. [Introduction](#introduction)
2. [Prerequisite](#prerequisite)
3. [Installation](#installation)
4. [Access](#access)
5. [Change Configuration](#change-configuration)
6. [Contributing](#contributing)

## **Introduction**

Miver is a minecraft server hosting service giving access to a web interface from which users can create an account in order to generate minecraft servers and be able to administer them from a web interface.

## **Prerequisite**

Machine using rocky linux 9 up to date

Open ports 50000 to 50010 and 22 and 80 and 443 and 19999 and redirect them to the machine

## **Installation**

Clone this git repository

Edit configuration

Execute the miver.sh file contained in this file using the sudo user

## **Access**

Accès to miver interface with the url : < yourServeurName >


## **Change Configuration**

To setup Nginx with your own server name, modify var of /srv/miver-web/nginx.conf :
```bash
server {
        listen             80;
        server_name        _;                           ## <-- HERE

...

    server {
        listen           443 ssl;
        server_name        _;                           ## <-- HERE
```


By default the ports assigned to minecraft servers are between 50000 and 50010
To modify them, go to /srv/miver-web/www/php/requestManager.php and change value of var :
```php
    $minServPort = 50000;
    $maxServPort = 50010;
```
And go to /srv/miverChangePort.sh modify value of var :
```bash
minPort=50000
maxPort=50010
```
Then execute /srv/miverChangePort.sh with sudo user

<br>
<br>

## **Contributing**

##### **MACE** Léo | **JACOLOT** Marine | **DAUBRESSE** Alexy